﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : MonoBehaviour
{
    CharacterController a;
    // Start is called before the first frame update
    void Start()
    {
        a=gameObject.GetComponent<CharacterController>();   
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        a.Move(move * Time.deltaTime * 1);
    }

}