﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dorong : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D a;
    
    void Start()
    {
        a = this.GetComponent<Rigidbody2D>();
        Invoke("dorong", 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void dorong() 
    {
        Vector3 arah = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward) * Vector3.left;
        a.AddForce(arah * 600);
    }

}
