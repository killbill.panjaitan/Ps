﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mpaw : MonoBehaviour
{
    GameObject r;
    SpawnRandom ro;
    Text assa;
    bool mulai = false;
    float current;
    float spawnn = 3;
    private void Awake()
    {
        r = GameObject.Find("spawn");
        ro = r.GetComponent<SpawnRandom>();
        assa = GameObject.Find("valoe").GetComponent<Text>();
    }

    private void Update()
    {
        assa.text = scorem.highScore.ToString();
        if (mulai)
        {
            current += Time.deltaTime;
            if (current > spawnn)
            {
                ro.move(this.transform);
                gameObject.GetComponent<Collider2D>().enabled = true;
                gameObject.GetComponent<SpriteRenderer>().enabled = true;
                mulai = false;
                current = 0;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            gameObject.GetComponent<Collider2D>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            scorem.highScore -= 1;
            mulai = true;
        }

    }

}
