﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandom : MonoBehaviour
{
    public GameObject[] a;
    public Transform k,b;
    // Start is called before the first frame update
    void Start()
    {
        spawn();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void spawn()
    {

        foreach (GameObject a in a)
        {
            Vector3 origin = k.position;
            Vector3 range = k.localScale / 3.0f;
            Vector3 randomRange = new Vector3(Random.Range(-range.x, range.x),
                                              Random.Range(-range.y, range.y));
            Vector3 randomCoordinate = origin + randomRange;
            
            bool spa = false;
            while (!spa)
            {
                if ((randomCoordinate - b.position).magnitude < 3)
                {
                   
                    randomRange = new Vector3(Random.Range(-range.x, range.x),
                                              Random.Range(-range.y, range.y));
                    randomCoordinate = origin + randomRange;
                    continue;
                }
                
                else 
                {
                    Instantiate(a, randomCoordinate, Quaternion.identity);
                    spa = true;
                }

            }

            }
    }

    public void move(Transform G)
    {
        Vector3 origin = k.position;
        Vector3 range = k.localScale / 3.0f;
        Vector3 randomRange = new Vector3(Random.Range(-range.x, range.x),
                                          Random.Range(-range.y, range.y));
        Vector3 randomCoordinate = origin + randomRange;
       

        bool spa = false;
        while (!spa)
        {
            if ((randomCoordinate - b.position).magnitude < 3)
            {

                randomRange = new Vector3(Random.Range(-range.x, range.x),
                                          Random.Range(-range.y, range.y));
                randomCoordinate = origin + randomRange;
                continue;
            }

            else
            {
                G.position = randomCoordinate;
                spa = true;
            }

        }

    }
}



